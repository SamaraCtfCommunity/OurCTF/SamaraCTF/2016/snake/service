#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "crash.h"
#include "ctf_greeting.h"
#include "ctf_flag_exchange.h"
#include "snake.h"

char *author = "di27mack";

void client_connection(void)
{
	char command[6];
	
	greeting();
	while(1)
	{
		read(0, command, 6);
		if (strstr(command, "$stop"))
			break;
		else if (strstr(command, "$play"))
			play();
		else if (strstr(command, "$pull"))
			pull();
		else if (strstr(command, "$push"))
			push();
	}
}

int main(int argc, char** argv)
{	
	int sock, client_socket, struct_addr_in_size = sizeof(struct sockaddr_in);
	struct sockaddr_in server_addr, client_addr;

	if (argc != 2) 
		crash("Usage: <servername> <port>");
	if((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) 
		crash("socket");
	
	server_addr.sin_family = AF_INET;
	server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_port = htons(atoi(argv[1]));
	
	if (bind(sock, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0) 
		crash("bind");
	if(listen(sock, 1) == -1) 
		crash("listen");
    while(1)
    {
    	if ((client_socket = accept(sock, (struct sockaddr*) &client_addr, (socklen_t*) &struct_addr_in_size)) < 0) 
			crash("accept");
    	switch(fork())
    	{
    		case -1: crash("fork"); break;
    		case  0:
    		{
    			close(sock);
				dup2(client_socket, 0);
				dup2(client_socket, 1);
				dup2(client_socket, 2);
				client_connection();
				close(client_socket);
				exit(0);
    		}
    		default: close(client_socket);
    	}
    }
	return 0;
}