#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h> 
#include "ctf_greeting.h"
#include "push.h"
#include "pull.h"
#include "play.h"
#include "play.h"

char author[] = "di27mack";

int main(void)
{
	char command[6];
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
	greeting();
	while(1)
	{
		read(0, command, 6);
		if (strstr(command, "$stop"))
			break;
		else if (strstr(command, "$play"))
			play();
		else if (strstr(command, "$pull"))
			pull();
		else if (strstr(command, "$push"))
			push();
	}
	return 0;
}