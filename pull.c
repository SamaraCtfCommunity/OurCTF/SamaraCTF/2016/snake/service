#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "crash.h"
#include "ctf_flag_exchange.h"

#define CONTENT_LENGTH 33
#define PACKAGE_LENGTH 53
#define FLAG_ID_LENGTH 4

int check_permission_v1_1(const char* flag, const char* content, int is_auth)
{
	char checksum[33], tmp_buffer[33];
	
	get_checksum(flag, checksum);
	sprintf(tmp_buffer, content);
	if (strstr(tmp_buffer, checksum))
		is_auth = 1;
	else 
		is_auth = 0;
	return is_auth;
}

int check_permission_v1_0(const char* flag, const char* content, int is_auth)
{
	char checksum[33], tmp_buffer[33];	

	get_checksum(flag, checksum);
	strncpy(tmp_buffer, content, 32);
	if(strstr(tmp_buffer, checksum))
		is_auth = 1;
	return is_auth;
}


typedef int (*ptr_check_access_function)(const char * flag, const char* content, int is_auth);
ptr_check_access_function check_access_function_array[3] = {check_permission_v1_0, check_permission_v1_1};

void pull(void)
{	
	int is_auth = 0, num_func;
	char flag[CONTENT_LENGTH + 1];
	char flag_id[FLAG_ID_LENGTH + 1];
	char checksum[CONTENT_LENGTH + 1];
	char package[PACKAGE_LENGTH + 1];
	
	int seed = (flag_id[0] | flag_id[1] | flag_id[2] | flag_id[3]) & 0x7fffffff;
	srand(seed);
	memset(package, 0, PACKAGE_LENGTH + 1);
	gets(package);
	memset(flag, 0, CONTENT_LENGTH + 1);
	memset(flag_id, 0, FLAG_ID_LENGTH + 1);
	package[PACKAGE_LENGTH] = '\0';         
	parse_request(package, flag_id, checksum);
	if (get_flag(flag_id, flag) != CONTENT_LENGTH)
		crash("invalid reading flag length");
	num_func = rand() % 2; 
	if ((is_auth = check_access_function_array[num_func](flag, checksum, is_auth)) == 0)
		crash("permission denied");
	compose_response(package, flag_id, flag);
	puts(package);
}