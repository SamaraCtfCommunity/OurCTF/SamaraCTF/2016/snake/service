#include <stdio.h>
#include <stdlib.h>

void crash(const char* msg)
{
	printf("ERROR: %s\n", msg);
	fflush(stdout);
	exit(1);
}
