from random import randint
from os import system
command = 'RIGHT'
score = 0
snake = [[4,10], [4,9], [4,8]]      
food = [10,20]                
print("FOOD: x=%d y=%d" % (food[0], food[1]))            
print("SNAKE: x=%d y=%d" % (snake[0][0], snake[0][1]))   
while 1 == 1:  
    prevcommand = command     
    command = input("COMMAND: ")
    if command == 'BREAK':
        break
    if command not in ['LEFT', 'RIGHT', 'UP', 'DOWN']:
        command = prevcommand
    snake.insert(0, [snake[0][0] + (command == 'DOWN' and 1) + (command == 'UP' and -1), snake[0][1] + (command == 'LEFT' and -1) + (command == 'RIGHT' and 1)])
    if snake[0][0] == 0: snake[0][0] = 18
    if snake[0][1] == 0: snake[0][1] = 58
    if snake[0][0] == 19: snake[0][0] = 1
    if snake[0][1] == 59: snake[0][1] = 1
    if snake[0] in snake[1:]: break
    if snake[0] == food:                  
        food = []
        score += 1
        while food == []:
            food = [randint(1, 18), randint(1, 58)]               
            if food in snake: 
				food = []
        print("FOOD: x=%d y=%d" % (food[0], food[1])) 
    print("SNAKE: x=%d y=%d" % (snake[0][0], snake[0][1]))
print("SCORE: %d" % score)
if score > 27:
    system("cat ./flags/flag#*")