#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "crash.h"
#include "ctf_flag_exchange.h"

#define CONTENT_LENGTH 33
#define PACKAGE_LENGTH 53
#define FLAG_ID_LENGTH 4

void push(void)
{
	char flag[CONTENT_LENGTH + 1];
	char flag_id[FLAG_ID_LENGTH + 1];
	char package[PACKAGE_LENGTH + 1];
	char checksum[CONTENT_LENGTH + 1];
	
	memset(flag, 0, CONTENT_LENGTH + 1);
	memset(flag_id, 0, FLAG_ID_LENGTH + 1);
	
	gets(package);
	parse_request(package, flag_id, flag);
	save_flag(flag_id, flag);
	get_checksum(flag, checksum);
	memset(package, 0, PACKAGE_LENGTH + 1);
	compose_response(package, flag_id, checksum);
	puts(package);
}