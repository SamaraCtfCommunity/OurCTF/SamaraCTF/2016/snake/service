#include <openssl/md5.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include "crash.h"

#define CONTENT_LENGTH 33
#define FILENAME_LENGTH 256
#define PACKAGE_LENGTH 53

int get_flag(const char* flag_id, char* flag)
{
	FILE* fd;
	char filename[FILENAME_LENGTH + 1];
	memset(filename, 0,  FILENAME_LENGTH + 1);
	strcat(filename, "./flags/flag#");
	strcat(filename, flag_id);
	if ((fd = fopen(filename, "r")) == NULL)
		crash("open file for reading");
	fread(flag, 1, CONTENT_LENGTH + 1, fd);
	fclose(fd);
	return strlen(flag);
}

void save_flag(const char* flag_id, const char* flag)
{
	FILE* fd;
	char filename[FILENAME_LENGTH + 1];
	memset(filename, 0, FILENAME_LENGTH + 1);
	strcat(filename, "./flags/flag#");
	strcat(filename, flag_id);
	if ((fd = fopen(filename, "w")) == NULL)
		crash("open file for writting");
	fwrite(flag, 1, CONTENT_LENGTH + 1, fd);
	fclose(fd);
}

void parse_request(const char* package, char* flag_id, char* content)
{
	strncpy(flag_id, package + 14, 4);
	strncpy(content, package + 19, 33);
}

void compose_response(char* package, const char* flag_id, const char* content)
{
	sprintf(package, "SamaraCTF2016#%s{%s}", flag_id, content);
}

void get_checksum(const char* content, char* checksum)
{
	MD5_CTX md5handler;
	unsigned char md5digest[MD5_DIGEST_LENGTH];

	MD5_Init(&md5handler);
    MD5_Update(&md5handler, content, CONTENT_LENGTH);
	MD5_Final(md5digest, &md5handler);

	for (int i = 0; i < MD5_DIGEST_LENGTH; i++) 
        sprintf(checksum + (i * 2), "%02x", md5digest[i]);
}
