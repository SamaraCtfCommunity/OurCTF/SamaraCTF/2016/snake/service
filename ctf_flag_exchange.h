int get_flag(const char* flag_id, char* flag);
void save_flag(const char* flag_id, const char* flag);
void parse_request(const char* package, char* flag_id, char* content);
void compose_response(char* package, const char* flag_id, const char* content);
void get_checksum(const char* content, char* checksum);