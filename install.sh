#!/bin/sh

useradd -U -M -s /bin/false snake

apt-get install -y python-dev
apt-get install -y libssl-dev
apt-get install -y socat
rm snake
make clean
cp snake.conf /etc/init/
make
