CC=gcc
CLEAN=rm -f
PROGRAM_NAME=snake

$(PROGRAM_NAME): main.o ctf_flag_exchange.o ctf_flag_exchange.h push.o push.h pull.o pull.h ctf_greeting.o ctf_greeting.h play.c play.h crash.o crash.h
	$(CC) -I"/usr/include/python2.7" -o $@ $^ -lcrypto -lpython2.7
	
main.o: main.c 
	$(CC) -c $^ -fstack-protector 
	
ctf_greeting.o: ctf_greeting.c
	$(CC) -c $^ -fstack-protector 
	
ctf_flag_exchange.o: ctf_flag_exchange.c
	$(CC) -c $^ -std=gnu11 -g -fstack-protector 
	
push.o: push.c
	$(CC) -c $^	-Wdeprecated-declarations
	
pull.o: pull.c
	$(CC) -c $^ -O0 -fstack-protector -Wdeprecated-declarations

play.o: play.c
	$(CC) -c $^ -fstack-protector 
	
crash.o: crash.c
	$(CC) -c $^ -fstack-protector 
	
clean: 
	$(CLEAN)  *.o snake
