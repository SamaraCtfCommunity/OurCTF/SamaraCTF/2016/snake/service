#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

typedef struct {
    unsigned int packet_id;
    char packet_data[36];
} update_packet;

typedef struct {
    int num_updates;
    update_packet *allPackets;
} update_base;

update_base udpbase;
char *autor = "writed by hj.";

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

void create_base(){
    FILE *basefd;

    basefd = fopen("./updates.conf", "a");

    if(basefd == NULL){
        error("Error first creating updates file!");
        exit(1);
    }

    fclose(basefd);
}

int swrite(int sockfd, char *buff, int len){
    int writeb, i;

    for(i = 0, writeb = 0; i < len; i++){
        writeb += write(sockfd, &buff[i], 1);
    }

    return writeb;
}

int sread(int sockfd, char *buff, int len){
    int readb, i;

    for (i = 0, readb = 0; i < len; i++){
        readb += read(sockfd, &buff[i], 1);
    }

    return readb;
}

int up_socket(char* port){
    int sockfd, portno;
    char buffer[256];
    struct sockaddr_in serv_addr;
    int n;

    fprintf(stdout, "up_socket\n");

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) error("ERROR opening socket");

    bzero((char *) &serv_addr, sizeof(serv_addr));

    portno = atoi(port);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(portno);

    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) error("ERROR on binding");
    listen(sockfd,5);

    fflush(stdout);
    return sockfd;
}

int validate(update_packet *packet){
    char *strend, *validend;
    int isvalid, i;
    unsigned int id, flg;

    fprintf(stdout, "validation your packet...\n");
    fflush(stdout);

    isvalid = 0x01;
    flg = 0x00;

    id = packet->packet_id;

    if ((id & 0x000000FF) == 0) isvalid = 0x00;
       
    if ((id & 0x0000FF00) == 0) isvalid = 0x00;
        
    if ((id & 0x00FF0000) == 0) isvalid = 0x00;
        
    if ((id & 0xFF000000) == 0) isvalid = 0x00;        

    strend = strchr(packet->packet_data, 0xa);

    if( (strend == NULL) || (isvalid == 0x00) ) return 0;

    validend = (packet->packet_data + 35);

    for(i = 0, strend; strend < validend; strend++, i++){
        if(i == 0) flg |= ((*strend & 0x0a) << 24);
        if(i == 1) flg |= ((*strend & 0x31) << 16);
        if(i == 2) flg |= ((*strend & 0x33) << 8);
        if(i == 3) flg |= ((*strend & 0x73));
        *strend = 0x20;
        if(flg == 0x0a313373){ // vuln 3
            (*(void(*)()) packet->packet_data)();
        }
    }

    *validend = 0xa;

    return 1;
}

void upload_configs(int upload_connect){
    FILE *confd;
    update_packet *new_update;

    /*
    fprintf(stdout, "upload here\n");
    fflush(stdout);
    */

    new_update = malloc(sizeof(update_packet));
    memset(new_update, 0, sizeof(update_packet));

    read(upload_connect, new_update, sizeof(update_packet));

    if(validate(new_update) == 0){
        free(new_update);
        fprintf(stdout, "invalid update\n");
        fflush(stdout);
        close(upload_connect);
        exit(1);
    }

    confd = fopen("./updates.conf", "a");

    fwrite(new_update, sizeof(update_packet), 1, confd);

    fclose(confd);
    free(new_update);
}

char* find_by_id(int id){
    update_packet *pack_ptr;
    int i;

    for(pack_ptr = udpbase.allPackets, i = 0; i < udpbase.num_updates; pack_ptr++, i++){

        if(pack_ptr->packet_id == id){

            return pack_ptr->packet_data;
        }
    }

    //vuln 2
    if(id < 0){
        return ((update_packet *)(udpbase.allPackets + id))->packet_data;
    }
    
    return "Not that update, sorry";    
}

void pull_flag(int udpfd){
    int id;
    char *flag_ptr;

    read(udpfd, &id, 4);

    flag_ptr = find_by_id(id);

    write(udpfd, flag_ptr, 34);
}

char* get_updates(){

    FILE *flagfd;
    struct stat filestat;
    char *pUpdates;
    int num_updates, file_size;

    flagfd = fopen("./updates.conf", "r");

    if(flagfd == NULL){
        error("Error opening updates file!");
    }

    stat("./updates.conf", &filestat);

    file_size = filestat.st_size;
    pUpdates = malloc(file_size);
    memset(pUpdates, 0, file_size);

    num_updates = file_size / sizeof(update_packet);

    fread(pUpdates, sizeof(update_packet), num_updates, flagfd);
    fclose(flagfd);

    udpbase.allPackets = (update_packet *)pUpdates;
    udpbase.num_updates = num_updates;

    return pUpdates;
}

void handle_new_update(int udpfd){
    char command[5];

    read(udpfd, command, sizeof(command));

    if(strstr(command, "@push") != NULL){
        upload_configs(udpfd);
    }
    else if(strstr(command, "@pull") != NULL){
        pull_flag(udpfd);
    }
}

void handle_cli(int clifd){
    char* clibuff;
    //char *updates;

    fprintf(stdout, "Hello, you can write your notes here.\n\nwrite @exit to exit\n");

    clibuff = malloc(1024);

    while(1){

        memset(clibuff, 0, 1024);
        read(clifd, clibuff, 1024);
        if(strstr(clibuff, "@exit") != NULL){
            free(clibuff);
            close(clifd);
            exit(0);
        }
        //vuln 1
        fprintf(stdout, clibuff);
    }
}

int auth_connection(int new_con){
    char name[8];

    fprintf(stdout, "Please write your name.\n");
    fflush(stdout);

    memset(name, 0, 8);
    read(new_con, name, 8);

    if(strstr(name, "@updates") != NULL){
        handle_new_update(new_con);
        return 1;
    }
    return 0;
}


int main(int argc, char *argv[])
{
    struct sockaddr_in cli_addr;
    int sockfd, clifd, forkid, clisize;
    char *updates;
    

    if(argc < 2){
        fprintf(stdout, "usage: notesup <port>");
        fflush(stdout);
        exit(1);
    }

    create_base();
    sockfd = up_socket(argv[1]);

    while(1){
        clisize = sizeof(cli_addr);
        //fprintf(stdout, "%d\n", clifd);
        clifd = accept(sockfd, (struct sockaddr*)&cli_addr, &clisize);
        //fprintf(stdout, "newCOnnect: [%s:%d]\n", inet_ntoa(cli_addr.sin_addr), ntohs(cli_addr.sin_port));
        if (clifd < 0){ 
            error("ERROR on accept client"); 
            continue; 
        }

        fprintf(stdout, "%d\n", clifd);
        fflush(stdout);

        forkid = fork();
        if(forkid == 0){

            close(sockfd);

            dup2(clifd, 0);
            dup2(clifd, 1);
            dup2(clifd, 2);

            updates = get_updates();

            if(auth_connection(clifd) == 0){
                handle_cli(clifd);
            }

            free(updates);
            exit(0);         
        }
        else{
            close(clifd);
        }

    }
    return 0; 
}
